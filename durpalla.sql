-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2020 at 07:36 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `durpalla`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_panels`
--

CREATE TABLE `admin_panels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_panels`
--

INSERT INTO `admin_panels` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$qmpS.WdHIFnEjDCO44iunumRauxyhOns3yEEOtZDhdzNgtevXU0YC', '2019-09-27 14:54:43', '2019-09-27 14:54:43');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` int(11) NOT NULL,
  `message` varchar(9999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `fine` int(11) DEFAULT NULL,
  `corporate` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `tracking`, `seat`, `message`, `promo_code`, `discount`, `fine`, `corporate`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 1577264475, 'R389008', 4, NULL, NULL, 0, 0, 0, 10000, 2, '2020-01-08 15:32:49', '2020-01-09 17:06:51'),
(2, 1577264475, 'R117008', 5, NULL, NULL, 0, 0, 0, 7950, 2, '2020-01-09 17:01:49', '2020-01-09 17:30:15'),
(3, 1577264475, 'R539001', 4, NULL, NULL, 0, 0, 0, 776, 2, '2020-01-09 18:18:57', '2020-01-20 12:04:10'),
(4, 1577264475, 'R190001', 5, NULL, NULL, 0, 0, 0, 8400, 2, '2020-01-09 18:20:52', '2020-01-20 12:04:10'),
(5, 1578593518, 'R979001', 1, NULL, NULL, 0, 0, 0, 1023, 1, '2020-01-10 00:20:36', '2020-01-10 00:20:36'),
(6, 1577264475, 'R597002', 3, NULL, NULL, 0, 0, 0, 3570, 1, '2020-01-20 12:05:15', '2020-01-20 12:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `booking_cancels`
--

CREATE TABLE `booking_cancels` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_plate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_image1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_image2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fuel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kilometers` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `brand_id`, `model`, `number_plate`, `car_image1`, `car_image2`, `fuel`, `kilometers`, `car_type`, `registration_year`, `model_year`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'allion', '1111111111111111', '411574184112.jpg', '181574184112.jpg', 'gas', '1234', 'Luxury', '2019', '2019', 1574184029, 1, '2019-11-19 23:21:52', '2019-11-19 23:23:34'),
(2, '4', 'extra', '222222', '381574184302.jpg', '531574184302.jpg', 'ACTEL', '34343', NULL, '2019', '2019', 1574184029, 1, '2019-11-19 23:25:02', '2019-12-31 16:18:17'),
(3, '1', 'allion', '3434343', '871574269943.jpg', '521574269943.jpg', 'gas', '1234', 'Luxury', '2019', '2019', 1574269389, 1, '2019-11-20 23:12:23', '2019-11-20 23:12:58'),
(4, '1', 'allion', '324234', '921574271293.jpg', '631574271293.jpg', 'ACTEL', '2345234', 'Luxury', '2019', '2019', 1574270962, 1, '2019-11-20 23:34:53', '2019-11-20 23:35:27'),
(5, '1', 'allion', '342423423', '111576738650.jpg', '291574404549.jpg', 'gas', '1234', 'Luxury', '2019', '2019', 1574404297, 1, '2019-11-22 12:35:49', '2019-11-22 12:36:27'),
(6, '1', 'allion', '545646', '111576738650.jpg', '611576738650.jpg', 'gas', '12', 'Comfort', '2015', '2011', 1576737619, 1, '2019-12-19 12:57:30', '2019-12-19 12:58:07'),
(7, '1', 'G corolla', '23423', '991577771462.jpg', '131577771462.jpg', 'gas', '234234', 'Comfort', '234234', '2019', 1575877737, 1, '2019-12-31 11:51:02', '2020-01-05 11:20:21'),
(8, '4', 'G corolla', '123456', '601578125763.jpg', '791578125763.jpg', 'gas', '5200', 'Luxury', '2015', '2019', 1577541597, 1, '2020-01-04 14:16:03', '2020-01-04 14:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `car_brands`
--

CREATE TABLE `car_brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_brands`
--

INSERT INTO `car_brands` (`id`, `brand_name`) VALUES
(1, 'Toyota'),
(2, 'Acura'),
(3, 'Audi'),
(4, 'BMW'),
(5, 'Bentley'),
(6, 'Buick');

-- --------------------------------------------------------

--
-- Table structure for table `close_accounts`
--

CREATE TABLE `close_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recommend` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `improve` varchar(9999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `close_accounts`
--

INSERT INTO `close_accounts` (`id`, `reason`, `recommend`, `improve`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', 'yes', 1574186461, '2019-11-20 00:13:16', '2019-11-20 00:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `corporates`
--

CREATE TABLE `corporates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_phone` int(11) NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `corporates`
--

INSERT INTO `corporates` (`id`, `name`, `address`, `c_name`, `c_phone`, `logo`, `discount`, `created_at`, `updated_at`) VALUES
(1, 'United IT', 'House 1287, Road 11, ave 02 Mirpur DOHS', 'sumon', 1716059030, '771570475492.jpg', 0, '2019-10-08 00:11:32', '2019-10-08 00:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_groups`
--

CREATE TABLE `corporate_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` int(11) NOT NULL,
  `corporate_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faulty_trips`
--

CREATE TABLE `faulty_trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faulty_trips`
--

INSERT INTO `faulty_trips` (`id`, `tracking`, `reason`, `created_at`, `updated_at`) VALUES
(1, 'R280001', 'Car damage', '2020-01-16 09:57:35', '2020-01-16 09:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `landing_images`
--

CREATE TABLE `landing_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approve` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `landing_images`
--

INSERT INTO `landing_images` (`id`, `image`, `approve`, `created_at`, `updated_at`) VALUES
(1, '631570446985.png', 0, '2019-10-07 16:16:25', '2019-10-09 11:54:05'),
(2, '971570447603.jpg', 1, '2019-10-07 16:26:43', '2019-10-09 11:54:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_21_062226_create_users_table', 1),
(2, '2019_02_21_110806_create_admin_panels_table', 1),
(3, '2019_02_24_104118_create_cars_table', 1),
(4, '2019_02_25_013851_create_request_rides_table', 1),
(5, '2019_02_25_043236_create_post_rides_table', 1),
(6, '2019_02_26_121825_create_stopovers_table', 1),
(7, '2019_03_01_032831_create_ride_settings_table', 1),
(8, '2019_03_03_150729_create_verifications_table', 1),
(9, '2019_03_07_174107_create_resources_table', 1),
(10, '2019_03_08_173947_create_promo_codes_table', 1),
(11, '2019_03_09_062046_create_post_ride_addresses_table', 1),
(12, '2019_03_10_214001_create_bookings_table', 1),
(13, '2019_03_11_060328_create_corporates_table', 1),
(14, '2019_03_11_143056_create_corporate_groups_table', 1),
(15, '2019_03_12_112600_create_booking_cancels_table', 1),
(16, '2019_03_26_174636_create_car_brands_table', 1),
(17, '2019_03_27_055636_create_close_accounts_table', 1),
(18, '2019_03_30_080135_create_landing_images_table', 1),
(19, '2019_04_05_143908_create_notifications_table', 1),
(20, '2019_04_30_180148_create_popular_rides_table', 1),
(21, '2019_05_01_033034_create_user_ratings_table', 1),
(22, '2019_05_05_191435_create_references_table', 1),
(23, '2020_01_16_153209_create_faulty_trips_table', 2),
(25, '2020_01_16_195856_create_post_time_updates_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_post` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `matching` varchar(9999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `user_post`, `matching`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'request', '2', 'R851001,R678004', 1, 1574404297, '2019-11-22 12:47:35', '2019-11-22 12:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `popular_rides`
--

CREATE TABLE `popular_rides` (
  `id` int(10) UNSIGNED NOT NULL,
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_rides`
--

CREATE TABLE `post_rides` (
  `id` int(10) UNSIGNED NOT NULL,
  `s_lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_time2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `return` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_time2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car_id` int(11) NOT NULL,
  `driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_rides`
--

INSERT INTO `post_rides` (`id`, `s_lat`, `s_lng`, `s_location`, `e_lat`, `e_lng`, `e_location`, `departure`, `d_time`, `d_time2`, `return`, `r_time`, `r_time2`, `seat`, `condition`, `car_id`, `driver`, `distance`, `duration`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(9, '23.7509735', '90.373582', 'Dhanmondi 32 Road Bridge, Dhanmondi Bridge, Dhaka, Bangladesh', '22.4715718', '91.7876793', 'University of Chittagong, Chittagong University Road, Hathazari, Bangladesh', '01/09/2020', '7', 'PM', NULL, NULL, NULL, '5', '1,3,4', 8, 'SP', '252 km', '5 hours 5 mins', 1577541597, 1, '2020-01-09 18:16:04', '2020-01-22 09:42:56'),
(10, '23.6238241', '90.49992689999999', 'Chasara Chotor, Narayanganj, Bangladesh', '25.7534962', '89.2492185', 'Rangpur Technical School & College, City Bazar Road, Rangpur, Bangladesh', '01/23/2020', '4', 'PM', NULL, NULL, NULL, '8', '1,2,3,4', 8, 'SP', '326 km', '8 hours 8 mins', 1577541597, 1, '2020-01-16 15:31:48', '2020-01-16 16:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `post_ride_addresses`
--

CREATE TABLE `post_ride_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_ride_addresses`
--

INSERT INTO `post_ride_addresses` (`id`, `lat`, `lng`, `location`, `serial`, `post_id`, `created_at`, `updated_at`) VALUES
(22, '23.7509735', '90.373582', 'Dhanmondi 32 Road Bridge, Dhanmondi Bridge, Dhaka, Bangladesh', 1, 9, '2020-01-09 18:16:04', '2020-01-09 18:16:04'),
(23, '23.6918777', '90.4814999', 'Sign Board, Shiddhirganj, Bangladesh', 2, 9, '2020-01-09 18:16:04', '2020-01-09 18:16:04'),
(24, '23.4672479', '91.1740341', 'Ration Store,Police Line,Cumilla, Comilla, Bangladesh', 3, 9, '2020-01-09 18:16:04', '2020-01-09 18:16:04'),
(25, '22.4715718', '91.7876793', 'University of Chittagong, Chittagong University Road, Hathazari, Bangladesh', 4, 9, '2020-01-09 18:16:04', '2020-01-09 18:16:04'),
(26, '23.6238241', '90.49992689999999', 'Chasara Chotor, Narayanganj, Bangladesh', 1, 10, '2020-01-16 15:31:48', '2020-01-16 15:31:48'),
(27, '24.2672516', '89.9193513', 'Tangail Collectorate Girls High School & College, Tangail, Bangladesh', 2, 10, '2020-01-16 15:31:48', '2020-01-16 15:31:48'),
(28, '24.82392340000001', '89.3805837', 'Police Lines School & College, Bogura, Bogra, Bangladesh', 3, 10, '2020-01-16 15:31:48', '2020-01-16 15:31:48'),
(29, '25.7534962', '89.2492185', 'Rangpur Technical School & College, City Bazar Road, Rangpur, Bangladesh', 4, 10, '2020-01-16 15:31:48', '2020-01-16 15:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `post_time_updates`
--

CREATE TABLE `post_time_updates` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `d_time2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `p_amount` int(11) NOT NULL,
  `h_amount` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r_area` int(11) NOT NULL,
  `s_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `p_amount`, `h_amount`, `code`, `lat`, `lng`, `location`, `r_area`, `s_date`, `e_date`, `publish`, `created_at`, `updated_at`) VALUES
(1, 5, 50, 'dfdfdfdf', '22.845641', '89.54032789999997', 'Khulna, Bangladesh', 20, '10/30/2019', '10/31/2019', 1, '2019-10-29 00:48:22', '2019-10-29 00:49:03'),
(2, 10, 20, '1222', '23.7341698', '90.39275020000002', 'Arts Faculty, Dhaka 1205, Bangladesh', 20, '11/19/2019', '11/26/2019', 1, '2019-11-19 00:10:08', '2019-11-19 00:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE `references` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profession` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `name`, `profession`, `phone`, `address`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'sayiful islam', 'business', '98789789789', 'hojkjdklfjdl', 1570558576, '2019-10-08 23:25:44', '2019-10-08 23:25:44');

-- --------------------------------------------------------

--
-- Table structure for table `rejectphotos`
--

CREATE TABLE `rejectphotos` (
  `id` int(40) NOT NULL,
  `reject_photo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rejectphotos`
--

INSERT INTO `rejectphotos` (`id`, `reject_photo`, `created_at`, `updated_at`) VALUES
(1, 'no ur photo', '2020-01-13 18:00:00', '2020-01-13 18:00:00'),
(2, 'photo', '2020-01-14 02:02:40', '2020-01-14 02:02:40');

-- --------------------------------------------------------

--
-- Table structure for table `reject_reasons`
--

CREATE TABLE `reject_reasons` (
  `id` int(100) NOT NULL,
  `reject_message` varchar(100) DEFAULT NULL,
  `reject_type` int(40) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reject_reasons`
--

INSERT INTO `reject_reasons` (`id`, `reject_message`, `reject_type`, `created_at`, `updated_at`) VALUES
(1, 'nid no problem', 1, '2020-01-05 12:31:36', '2020-01-05 12:31:36'),
(2, 'nid image 1st problem', 1, '2020-01-05 06:40:51', '2020-01-05 06:40:51'),
(3, 'nid image 2nd problem', 1, '2020-01-05 18:54:40', '2020-01-05 18:54:40'),
(4, 'passport 2nd image problem', 1, '2020-01-06 18:56:03', '2020-01-06 18:56:03'),
(5, 'kk', 1, '2020-01-07 23:04:19', '2020-01-07 23:04:19'),
(6, 'jjjjj', 1, '2020-01-07 23:04:36', '2020-01-07 23:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `request_rides`
--

CREATE TABLE `request_rides` (
  `id` int(10) UNSIGNED NOT NULL,
  `s_lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `e_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `after` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_rides`
--

INSERT INTO `request_rides` (`id`, `s_lat`, `s_lng`, `s_location`, `e_lat`, `e_lng`, `e_location`, `after`, `before`, `seat`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '23.8497202', '90.40315190000001', 'Dhaka Airport, Dhaka, Bangladesh', '22.8129436', '89.56626959999994', 'Khulna City, Khulna, Bangladesh', '10/10/2019', '10/11/2019', '1', 1570129289, '2019-10-04 00:13:59', '2019-10-04 00:13:59'),
(2, '23.7341698', '90.39275020000002', 'Dhaka University, Dhaka, Bangladesh', '22.8031049', '89.53230959999996', 'Khulna University, Sher-E-Bangla Road, Khulna, Bangladesh', '11/30/2019', '11/30/2019', '1', 1574404297, '2019-11-22 12:47:35', '2019-11-22 12:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nid_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `name`, `phone`, `image`, `national_id`, `nid_image1`, `nid_image2`, `resource_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'sujon', '01994807111', '371569866368.jpg', '23423423423', '381569866368.png', '991569866368.jpg', 1569866368, 0, 1569866291, '2019-09-30 22:59:28', '2019-09-30 22:59:28'),
(2, 'sayiful islam', '01971335588', '861570558949.jpg', '2323223232', '891570558949.jpg', '821570558949.jpg', 1570558949, 3, 1570558576, '2019-10-08 23:22:29', '2019-10-08 23:23:20'),
(3, 'sujon', '01197352024', '601578419272.JPEG', '12321312312312312', '621578419272.JPEG', '1001578419272.jpg', 1578419272, 3, 1575877737, '2020-01-07 23:47:52', '2020-01-07 23:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `ride_settings`
--

CREATE TABLE `ride_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `search` int(11) NOT NULL DEFAULT '0',
  `min_price` int(11) NOT NULL DEFAULT '0',
  `commission` int(11) NOT NULL DEFAULT '0',
  `fine_6h` int(11) NOT NULL DEFAULT '0',
  `fine_12h` int(11) NOT NULL DEFAULT '0',
  `fine_12_upper` int(11) NOT NULL DEFAULT '0',
  `km_1st` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `price2` int(11) NOT NULL DEFAULT '0',
  `pkm_1st` int(11) NOT NULL DEFAULT '0',
  `pprice` int(11) NOT NULL DEFAULT '0',
  `pprice2` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ride_settings`
--

INSERT INTO `ride_settings` (`id`, `search`, `min_price`, `commission`, `fine_6h`, `fine_12h`, `fine_12_upper`, `km_1st`, `price`, `price2`, `pkm_1st`, `pprice`, `pprice2`, `created_at`, `updated_at`) VALUES
(1, 50, 20, 2, 50, 30, 50, 10, 10, 5, 10, 15, 10, '2019-09-27 14:55:02', '2019-11-19 23:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `stopovers`
--

CREATE TABLE `stopovers` (
  `id` int(10) UNSIGNED NOT NULL,
  `going` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `seat` int(11) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etime2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stopovers`
--

INSERT INTO `stopovers` (`id`, `going`, `target`, `price`, `seat`, `post_id`, `date`, `time`, `time2`, `distance`, `duration`, `edate`, `etime`, `etime2`, `status`, `payment`, `tracking`, `created_at`, `updated_at`) VALUES
(21, 1, 2, 194, 4, 9, '01/09/2020', '3', 'PM', '14.4 km', '37 mins', '01/09/2020', '06', 'PM', '3', '776', 'R539001', '2020-01-09 18:16:05', '2020-01-09 18:25:17'),
(22, 1, 3, 1023, 1, 9, '01/09/2020', '7', 'PM', '97.3 km', '2 hours 16 mins', '01/09/2020', '09', 'PM', '3', '1023', 'R979001', '2020-01-09 18:16:05', '2020-01-11 09:52:56'),
(23, 1, 4, 2570, NULL, 9, '01/09/2020', '7', 'PM', '252 km', '5 hours 5 mins', '01/10/2020', '12', 'AM', '3', '0', 'R902001', '2020-01-09 18:16:06', '2020-01-11 10:01:38'),
(24, 2, 3, 916, NULL, 9, '01/09/2020', '7', 'PM', '86.6 km', '1 hour 54 mins', '01/09/2020', '08', 'PM', '2', '0', 'R112001', '2020-01-09 18:16:06', '2020-01-11 09:35:48'),
(25, 2, 4, 2460, NULL, 9, '01/09/2020', '7', 'PM', '241 km', '4 hours 43 mins', '01/09/2020', '11', 'PM', '4', '0', 'R280001', '2020-01-09 18:16:06', '2020-01-16 10:03:51'),
(26, 3, 4, 1680, 5, 9, '01/09/2020', '4', 'PM', '163 km', '3 hours 9 mins', '01/09/2020', '6', 'PM', '3', '8400', 'R190001', '2020-01-09 18:16:06', '2020-01-09 18:26:05'),
(27, 1, 2, 1190, 3, 10, '01/23/2020', '4', 'PM', '114 km', '3 hours 23 mins', '01/23/2020', '07', 'PM', '0', '3570', 'R597002', '2020-01-16 15:31:48', '2020-01-20 12:05:15'),
(28, 1, 3, 2210, NULL, 10, '01/23/2020', '4', 'PM', '216 km', '5 hours 43 mins', '01/23/2020', '09', 'PM', '0', '0', 'R220002', '2020-01-16 15:31:48', '2020-01-16 16:06:16'),
(29, 1, 4, 3310, NULL, 10, '01/23/2020', '4', 'PM', '326 km', '8 hours 8 mins', '01/24/2020', '12', 'AM', '0', '0', 'R108002', '2020-01-16 15:31:49', '2020-01-16 16:06:16'),
(30, 2, 3, 1110, NULL, 10, '01/23/2020', '4', 'PM', '106 km', '2 hours 31 mins', '01/23/2020', '06', 'PM', '0', '0', 'R225002', '2020-01-16 15:31:49', '2020-01-16 16:06:16'),
(31, 2, 4, 2220, NULL, 10, '01/23/2020', '4', 'PM', '217 km', '4 hours 57 mins', '01/23/2020', '08', 'PM', '0', '0', 'R336002', '2020-01-16 15:31:49', '2020-01-16 16:06:16'),
(32, 3, 4, 1180, NULL, 10, '01/23/2020', '4', 'PM', '113 km', '2 hours 45 mins', '01/23/2020', '06', 'PM', '0', '0', 'R289002', '2020-01-16 15:31:50', '2020-01-16 16:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_verify` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `logincount` int(40) NOT NULL DEFAULT '1',
  `profile_general_biography` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rejectphoto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_status` int(40) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `phone`, `email`, `phone_verify`, `name`, `lname`, `day`, `month`, `year`, `gender`, `user_id`, `image`, `password`, `token`, `facebook_id`, `status`, `logincount`, `profile_general_biography`, `rejectphoto`, `photo_status`, `created_at`, `updated_at`) VALUES
(1, '001971335588', NULL, NULL, 'sayiful islam', NULL, '28', 'July', '1981', 'Male', 1569866291, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$nZITuv3wtUFY8XAHmFgRBOzs936IB9gXI.SPK6VVqy28fkz3LAknu', '1vL1xGZZG9NIUDWYnulc2pa8zUabAnlbYtA55xcx', NULL, 0, 1, NULL, NULL, 0, '2019-10-01 09:58:11', '2019-10-01 09:58:11'),
(2, '001925964018', NULL, NULL, 'Leon', NULL, '1', 'January', '1990', 'Male', 1570083448, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$U8y24PwREk.lTZccS4sYEeNjw6rjigzLaz.dXa6uhfNy0rdkEluFO', 'yUQsC8OCPP5Uh8yUjKS3a6fI4RhWN4h9D7Exloio', NULL, 0, 1, NULL, NULL, 0, '2019-10-03 22:17:28', '2019-10-03 22:17:28'),
(3, '001971335588', NULL, NULL, 'sayiful islam', NULL, '24', 'February', '1950', 'Male', 1570129289, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$m8osCy.PHAqbzNVdUG0AZu0Q379J4AVYWN9gs2KUFDmx4lcRUTEBO', 'PWs5LHitIrVxZSYb3ZuXWzLPofB4Z5uDnf6tSMiP', NULL, 0, 1, NULL, NULL, 0, '2019-10-04 11:01:29', '2019-10-04 11:01:29'),
(4, '001971335588', NULL, NULL, 'sayiful islam', NULL, '10', 'January', '1953', 'Male', 1570478927, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$HzLgDZxoOCaxll5jJosQVu9EPvgBifllztj347g4HsSrZHlL81qEe', 'sw1m6byGmZGJFvpyqi3DDJSm4mZPeG1OxwpPteGS', NULL, 0, 1, NULL, NULL, 0, '2019-10-08 12:08:47', '2019-10-08 12:08:47'),
(5, '001917335588', NULL, NULL, 'sayiful islam', NULL, '10', 'February', '1952', 'Male', 1570558576, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$icqqAMd69vGs/NvTGh0I8u4/b7NrlXhjZhZ5ZYDHBo3oGkk.rLYD2', 'UgV63YZKlqOAFLeYFeXY3gXwEQ7JALQNO1wNUqmt', NULL, 0, 1, NULL, NULL, 0, '2019-10-09 10:16:16', '2019-10-09 10:16:16'),
(6, '001717796906', NULL, NULL, 'Arifur Rahman', NULL, '20', 'July', '1991', 'Male', 1570620407, 'http://uconsultants.xyz/storage/user/961570620749.png', '$2y$10$7fMOzW4S28dJFexadbImNeAjGNcZ7YvS/yr5K1kyTkOCMlisThayK', 'ZjMRmJUNcawK7tsIPwY75r68dcGDLzKQOMBAZ4yK', NULL, 0, 1, NULL, NULL, 0, '2019-10-10 03:26:47', '2019-10-10 03:32:29'),
(7, '00123456789', NULL, NULL, 'sadik khan', NULL, '4', 'March', '1967', 'Male', 1570621281, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$gGqSymDuZQCUxKyk31d2demGPEd9kddyvqGB5I6U2YwQ7Zq.2Gv3e', 'ZjMRmJUNcawK7tsIPwY75r68dcGDLzKQOMBAZ4yK', NULL, 0, 1, NULL, NULL, 0, '2019-10-10 03:41:21', '2019-10-10 03:41:21'),
(8, '001993483438', NULL, NULL, 'sayiful islam', NULL, '1', 'February', '1951', 'Male', 1570963519, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$lhsahSa4fAIEDYsCTd9eeuVg2F6UlUWeknmelYpWi8tXdajBbjX4q', 'QkkqyHL39oEYuzs9fjAtevwsrgODhM5p7YA9Wekw', NULL, 0, 1, NULL, NULL, 0, '2019-10-14 02:45:19', '2019-10-14 02:45:19'),
(9, '001971335588', NULL, NULL, 'sumon', NULL, '1', 'February', '1952', 'Male', 1572116418, 'http://uconsultants.xyz/images/admin.jpg', '$2y$10$umi9.SSs36QDEKsiF0es7eb1kfOvInYupx6QbxDhwIAlYBjkcNCDK', 'Jp6LWG1iOjZT2g195P34st8XJfMNtPTH344CeoDg', NULL, 0, 1, NULL, NULL, 0, '2019-10-27 11:00:18', '2019-10-27 11:00:18'),
(10, '1234', 'chayan@gmail.com', NULL, 'chayan', 'majumder', '1', 'january', '1999', 'male', 1575444058, 'http://durpalla.xyz/ch/storage/user/151579001670.png', '$2y$10$IwIRmc/YUncoaLaHcZVT9uRWr0ow7uki4XXeMLhLtWCVENugx3m16', 'qLgptqArwPbNVkSCb9eAvOJ0FYYbBJ4DQJ3hc2OD', NULL, 0, 8, 'good', NULL, 0, '2019-12-04 01:20:58', '2020-01-14 17:34:30'),
(11, '1122', 'c@gmail.com', NULL, 'ch', 'ma', '1', 'January', '1950', 'male', 1575523683, 'http://localhost/durpalla/images/admin.jpg', '$2y$10$pyadJiDvz1lP5mQ9OC7FZetQspQvaIB5Lexf9sXZ64eb0CIRLi/2G', 'gXsi2FX4gDC7ecDogfYDt0dkTOAiXQ4gyogZN4R8', NULL, 0, 1, NULL, NULL, 0, '2019-12-04 23:28:03', '2019-12-04 23:29:27'),
(12, '0197133355588', NULL, NULL, 'sayiful', 'islam', '5', 'January', '1951', 'male', 1575876699, 'http://localhost/durpalla/images/admin.jpg', '$2y$10$zN1.Gr811CQrsRaSnrJV2ell1Mg1aDh9W7VMbvXGdOpgqGNnYFrvu', 'HB0XjN8PLtFwI42ikP3DMc8HcUVRHdizM3wG0f5K', NULL, 0, 1, NULL, NULL, 0, '2019-12-09 01:31:39', '2019-12-09 01:31:39'),
(13, '01971335588', NULL, NULL, 'sayiful', 'islam', '1', 'February', '1952', 'male', 1575877737, 'http://durpalla.xyz/durpalla_uconsal/storage/user/341576085336.jpg', '$2y$10$r0Zncefu7fjKIkRnzPkwVujuKYXnlwayS44UVyEJvAGCkBNcANSEK', 'HB0XjN8PLtFwI42ikP3DMc8HcUVRHdizM3wG0f5K', NULL, 0, 2, NULL, NULL, 0, '2019-12-09 01:48:57', '2020-01-07 22:50:53'),
(14, '01740', 'cm@gmail.com', NULL, 'chayan', 'majumder', '1', 'January', '1950', 'male', 1575953527, 'http://localhost/uconsultants/images/admin.jpg', '$2y$10$c1s363cFuYjvUrhmG3v98uGSbFSPoQbRbc9J.vkGOqIrZwjAJT26m', 'itE2OJQ2BfB6Yct9hoLtn0Ybjf0rzOhlD6RrgUep', NULL, 0, 1, NULL, NULL, 0, '2019-12-09 22:52:07', '2019-12-09 23:07:21'),
(15, '01740', 'chayan@gmail.com', NULL, 'chayan', 'majumder', '1', 'January', '1950', 'male', 1575960362, 'http://localhost/durpalla_uconsal/images/admin.jpg', '$2y$10$wjzXkDotmi3v4wLrNy5PSuR.hgofoPFnLNO/pPIbVSuQ.cYwqKC8O', '8h9jvSNXOiLgVbpfWKSUWyTQKM74XpZiIJjQPfZU', NULL, 0, 1, NULL, NULL, 0, '2019-12-10 00:46:02', '2019-12-10 00:52:09'),
(16, '01740', 'chayan@gmail.com', NULL, 'chayan', 'majumder', '1', 'January', '1950', 'male', 1575972158, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$Lcr40eo.ANl5OGKYmRHVuuZ4Ei5r9dSr3cNWEAAmlFCyKWki28cFS', 'bBYBfvylq4fQ98dozj49xsoi1dN6vTRQf2gGJB46', NULL, 0, 1, NULL, NULL, 0, '2019-12-10 16:02:38', '2019-12-10 16:03:04'),
(17, '01971335588', NULL, NULL, 'Sayiful', 'islam', '1', 'April', '1951', 'male', 1576085210, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$x3rK2Gh/x6FjYvrcIVFMVuOvde2M7PlY1hi0GkJBdzauxF5QQiUCG', 'lWQFPLT1YphIOHL0XAz9cGEKNeE27lEBap1E4uTQ', NULL, 0, 1, NULL, NULL, 0, '2019-12-11 23:26:50', '2019-12-11 23:26:50'),
(18, '01971335588', NULL, NULL, 'dfdf', 'dfdf', '1', 'February', '1952', 'male', 1576087430, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$eAvRgKGr4FiEHZEzPvB3turGjl5hGKOQxqVEYFeI1e8zGoqKW1fPO', 'lWQFPLT1YphIOHL0XAz9cGEKNeE27lEBap1E4uTQ', NULL, 0, 1, NULL, NULL, 0, '2019-12-12 00:03:50', '2019-12-12 00:03:50'),
(19, '01971335588', NULL, NULL, 'Sayiful', 'Islam', '1', 'February', '1951', 'male', 1576606777, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$xL3cva8IVLC/L01jJOIMguG5P.FKuZRLQmrR9SFOkVbPgXyDDHpzG', 'hd4Ug4JjCL2xOLYngYz6AJE9THiAKvXxAxFw9lkx', NULL, 0, 1, NULL, NULL, 0, '2019-12-18 00:19:37', '2019-12-18 00:19:37'),
(20, '01971335588', NULL, NULL, 'say', 'isl', '6', 'February', '1952', 'female', 1576606870, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$gn9zZO8mvDomVZ51SgJiJ.BlYXRj2aNMOl7NzaMm0qGdqELptMUlG', 'hd4Ug4JjCL2xOLYngYz6AJE9THiAKvXxAxFw9lkx', NULL, 0, 1, NULL, NULL, 0, '2019-12-18 00:21:11', '2019-12-18 00:21:11'),
(21, '01971335588', NULL, NULL, 'sayiful', 'islam', '1', 'February', '1951', 'male', 1576642682, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$tnVn08z5CVxV40FKhqiC.usYYXmet7YrGkChU8OPG0WNhpfZebhm.', 'wekljTPhKFeMkTfVFZh4gPgBMn7GhWRJrOnn3hS9', NULL, 0, 1, NULL, NULL, 0, '2019-12-18 10:18:02', '2019-12-18 10:18:02'),
(22, NULL, 'chayan121@gmail.com', NULL, 'chayan', 'majumder', '1', 'January', '1950', 'male', 1576645279, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$oOxPrTCldxazAHduZ1WbJOcIg/ujRCdjeZVNJiSEHQHjEVnnrmzoS', 'PEnVqhLxmH1RdkQMfm9ZUATHo03vxnlPNlq6lCak', NULL, 0, 1, NULL, NULL, 0, '2019-12-18 11:01:19', '2019-12-18 11:01:19'),
(23, '01716059030', NULL, NULL, 'sayiful', 'islam', '1', 'January', '1950', 'male', 1576697755, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$vGji4pKsVxwJgdstBj1jl.PcOxNmkZNiZDWwTObNmk2c.k5TYPop2', 'mUvuXUk3pZ2ijs6ajq3MoyYA8nJi79sfJ7uLwkAQ', NULL, 0, 1, NULL, NULL, 0, '2019-12-19 01:35:55', '2019-12-19 01:35:55'),
(24, NULL, 'chayanq@gmail.com', NULL, 'chayan', 'majumder', '1', 'January', '1950', 'male', 1576737619, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$zgYsngfwt39q6EztStriEuuKDxq6q3SAzv.xRaXUJZiLNo1oBbeBu', 'R6UMUYvacy1rZv4AUitrXbqJeV17uU8y4lbkUG8A', NULL, 0, 1, NULL, NULL, 0, '2019-12-19 12:40:19', '2019-12-19 12:40:19'),
(25, '01919551811', NULL, NULL, 'Farhan', 'Ahmed', '13', 'February', '1991', 'male', 1576758168, 'http://durpalla.xyz/durpalla_uconsal/storage/user/341576758368.jpg', '$2y$10$rNLgtR6f39Elf9mJOmEbLOnGI0KL1RFb0UNJGPkhm.iEfbL5gXM/S', 'O5jDPDrD5CR8qZEEHuyVGs68TewCl34l9IvoNHDh', NULL, 0, 1, NULL, NULL, 0, '2019-12-19 18:22:48', '2019-12-19 18:26:08'),
(26, '01917331018', NULL, NULL, 'Farhan', 'Ahmed', '1', 'February', '1951', 'male', 1577014903, 'http://durpalla.xyz/durpalla_uconsal/images/admin.jpg', '$2y$10$bob2lOt1.0PljVeMiaI.AeunQjIMKZF7OB9xB0WH2xz0.rogJEsBi', 'L3xoXrvCVuv27x8hrzCueNzI0Sz5reUGJbm4pa4v', NULL, 0, 2, NULL, NULL, 0, '2019-12-22 17:41:43', '2020-01-11 10:21:57'),
(27, '01627440390', NULL, NULL, 'hakim', 'sagor', '1', 'March', '1951', 'male', 1577264475, 'http://durpalla.xyz/ch/storage/user/931578475531.jpg', '$2y$10$pXmkPE11y9oT.LO6phMeY.86Ssc4r9.zTk.9Tw5f2m/FRP2UFyody', 'U7bMejkCAXSjmg4CnJVAji83WuJIgpQVQs7kAHTX', NULL, 0, 4, NULL, NULL, 0, '2019-12-25 15:01:15', '2020-01-20 12:04:02'),
(28, '009623189889', NULL, NULL, 'Abul Kalam Azad', '', 'o', 'January', '1952', 'Male', 1577541597, 'http://durpalla.xyz/ch/storage/user/411578127364.jpg', '$2y$10$pXmkPE11y9oT.LO6phMeY.86Ssc4r9.zTk.9Tw5f2m/FRP2UFyody', 'Q1HMKLTj4FfGCgTFzVHs00cdNsw7gnHbpq7OiPWj', NULL, 0, 5, NULL, NULL, 0, '2019-12-25 15:01:15', '2020-01-22 04:54:40'),
(29, '01719973149', NULL, NULL, 'nur', 'hossen', '2', 'April', '1988', 'male', 1578126960, 'http://durpalla.xyz/ch/images/admin.jpg', '$2y$10$MU2BigXDrzcc3WkNHOGVZuzA0Fx479n4t0Dt0IsauRIOUnQUstaV2', 'xLupszZsN1SPnvyCQDeThMqUPTV2VKuw11R6YR8q', NULL, 0, 1, NULL, NULL, 0, '2020-01-04 14:36:00', '2020-01-04 14:36:00'),
(30, NULL, 'freeset33@gmail.com', NULL, 'Tanka Nath', 'Sharma', '12', 'January', '1985', 'male', 1578211383, 'http://durpalla.xyz/ch/images/admin.jpg', '$2y$10$vFQ4OwnNS4HDCEf8WD5NFeIja.q6Edp4m2Xru8PNKrN/0EobPSKQC', 'X1AXHGz08k4Pf4Iho9iCPNvnIdepHyAYX4o4sBm2', NULL, 0, 1, NULL, NULL, 0, '2020-01-05 14:03:03', '2020-01-05 14:03:03'),
(31, '01713273242', NULL, NULL, 'nazmul', 'Hossain', '1', 'January', '1951', 'male', 1578593518, 'http://durpalla.xyz/ch/images/admin.jpg', '$2y$10$uJ/5VRM/Sda2TwE3eG6nyOkCCXfamPTLm39KrJWO1VCS3pGlfhoMe', '9qDhaKLFpU2dd1C9PgxtlZltrXoImtmT7yB96pYh', NULL, 0, 1, NULL, NULL, 0, '2020-01-10 00:11:58', '2020-01-10 00:11:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_ratings`
--

CREATE TABLE `user_ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `tracking` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(9999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_ratings`
--

INSERT INTO `user_ratings` (`id`, `user_id`, `tracking`, `rating`, `created_at`, `updated_at`) VALUES
(2, 1577541597, 'R190001', '{\"1577264475\":\"4\"}', '2020-01-09 18:26:30', '2020-01-09 18:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `nid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid_status` tinyint(1) NOT NULL DEFAULT '0',
  `passport` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_status` tinyint(1) NOT NULL DEFAULT '0',
  `driving` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_image1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_image2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driving_status` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rejected_message` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `nid`, `nid_image1`, `nid_image2`, `nid_status`, `passport`, `passport_image1`, `passport_image2`, `passport_status`, `driving`, `driving_image1`, `driving_image2`, `driving_status`, `email`, `phone`, `user_id`, `created_at`, `updated_at`, `rejected_message`) VALUES
(1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 2, NULL, NULL, 1569866291, '2019-09-30 23:02:23', '2020-01-05 13:12:52', 'Passport No Problem'),
(2, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, 1570129289, '2019-10-04 00:22:10', '2020-01-07 23:38:32', NULL),
(3, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1570478927, '2019-10-08 01:13:23', '2019-10-08 01:13:23', NULL),
(4, '1234567891234', '981570620598.png', '371570620598.png', 1, '123456789456123', '631570620634.png', '691570620634.png', 1, '123456789', '321570620677.png', '681570620677.png', 1, 'admin@admin.com', '01717796906', 1570620407, '2019-10-09 16:29:00', '2019-10-09 16:35:08', NULL),
(5, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1570621281, '2019-10-09 16:41:41', '2019-10-09 16:41:41', NULL),
(6, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1570963519, '2019-10-13 15:47:06', '2019-10-13 15:47:06', NULL),
(7, '11111111111111', '371574184582.jpg', '651574184582.jpg', 1, '767678', '281574184805.jpg', '811574184805.jpg', 1, NULL, NULL, NULL, 0, NULL, '01971335588', 1574184029, '2019-11-19 23:29:20', '2019-11-19 23:37:16', NULL),
(8, '3333333333333333333333333', '171574269453.jpg', '911574269453.jpg', 1, '5454', '941574269473.jpg', '991574269473.jpg', 1, '43534', '601574269496.jpg', '201574269496.jpg', 1, 'sayiful@gmail.com', '01716059030', 1574269389, '2019-11-20 23:03:46', '2019-11-20 23:21:17', NULL),
(9, '55555555555556565', '631574270996.jpg', '351574270996.jpg', 1, '75765476', '741574271017.jpg', '731574271017.jpg', 1, NULL, NULL, NULL, 0, 'sayiful@gmail.com', '01716059030', 1574270962, '2019-11-20 23:29:36', '2019-11-20 23:31:34', NULL),
(10, '87678786576', '701574404371.jpg', '951574404371.jpg', 1, '65476476', '821574404349.jpg', '631574404349.jpg', 1, '23423432', '131574404755.jpg', '701574404755.jpg', 1, 'sayiful2000@gmail.com', '01716059030', 1574404297, '2019-11-22 12:31:55', '2019-11-22 12:39:45', NULL),
(11, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1574410765, '2019-11-22 14:19:37', '2019-11-22 14:19:37', NULL),
(12, '2423543543', '351576731241.jpg', '311576731241.jpg', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, '01971335588', 1575877737, '2019-12-11 23:31:04', '2020-01-05 11:30:00', NULL),
(13, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1576606777, '2019-12-18 00:20:12', '2019-12-18 00:20:12', NULL),
(14, NULL, NULL, NULL, 0, '234534543345', '661576643465.jpg', '331576643465.jpg', 1, NULL, NULL, NULL, 0, NULL, NULL, 1576642682, '2019-12-18 10:24:15', '2019-12-19 12:47:00', NULL),
(15, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1576645279, '2019-12-18 11:05:21', '2019-12-18 11:05:21', NULL),
(16, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1576697755, '2019-12-19 01:36:04', '2019-12-19 01:36:04', NULL),
(17, '1111', '951576737920.jpg', '361576737920.jpg', 1, '1111', '911576738347.jpg', '881576738347.jpg', 1, '1111', '611576738431.jpg', '621576738431.jpg', 1, 'chayanq@gmail.com', '01740', 1576737619, '2019-12-19 12:44:48', '2019-12-19 12:55:03', NULL),
(18, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, '01919551811', 1576758168, '2019-12-19 18:23:42', '2019-12-19 18:25:10', NULL),
(19, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1575444058, '2019-12-22 17:20:22', '2019-12-22 17:20:22', NULL),
(20, '234234234234', '311577014952.jpg', '651577014952.jpg', 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1577014903, '2019-12-22 17:41:58', '2019-12-22 17:42:32', NULL),
(22, '756754', '311576731241.jpg', '311577014952.jpg', 1, '7686758976', '551577538366.webp', '821577538366.jpg', 1, NULL, NULL, NULL, 0, NULL, '766799876', 1577541597, NULL, NULL, NULL),
(23, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1578593518, '2020-01-10 00:14:15', '2020-01-10 00:14:15', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_panels`
--
ALTER TABLE `admin_panels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_cancels`
--
ALTER TABLE `booking_cancels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_brands`
--
ALTER TABLE `car_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `close_accounts`
--
ALTER TABLE `close_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporates`
--
ALTER TABLE `corporates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate_groups`
--
ALTER TABLE `corporate_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faulty_trips`
--
ALTER TABLE `faulty_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landing_images`
--
ALTER TABLE `landing_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_rides`
--
ALTER TABLE `popular_rides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_rides`
--
ALTER TABLE `post_rides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_ride_addresses`
--
ALTER TABLE `post_ride_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_time_updates`
--
ALTER TABLE `post_time_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `references`
--
ALTER TABLE `references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rejectphotos`
--
ALTER TABLE `rejectphotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reject_reasons`
--
ALTER TABLE `reject_reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_rides`
--
ALTER TABLE `request_rides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_settings`
--
ALTER TABLE `ride_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stopovers`
--
ALTER TABLE `stopovers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_ratings`
--
ALTER TABLE `user_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_panels`
--
ALTER TABLE `admin_panels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `booking_cancels`
--
ALTER TABLE `booking_cancels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `car_brands`
--
ALTER TABLE `car_brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `close_accounts`
--
ALTER TABLE `close_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `corporates`
--
ALTER TABLE `corporates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `corporate_groups`
--
ALTER TABLE `corporate_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faulty_trips`
--
ALTER TABLE `faulty_trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `landing_images`
--
ALTER TABLE `landing_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `popular_rides`
--
ALTER TABLE `popular_rides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_rides`
--
ALTER TABLE `post_rides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `post_ride_addresses`
--
ALTER TABLE `post_ride_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `post_time_updates`
--
ALTER TABLE `post_time_updates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `references`
--
ALTER TABLE `references`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rejectphotos`
--
ALTER TABLE `rejectphotos`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reject_reasons`
--
ALTER TABLE `reject_reasons`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `request_rides`
--
ALTER TABLE `request_rides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ride_settings`
--
ALTER TABLE `ride_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stopovers`
--
ALTER TABLE `stopovers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user_ratings`
--
ALTER TABLE `user_ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
