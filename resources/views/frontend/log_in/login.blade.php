@extends('frontend.layout.app')
@section('content')


<section class="section-login">

    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-5 col-xl-4 mx-auto">
                <div class="form-wrapper">
                    <h3 class="text-center mb-30">Log in</h3>
                    <form class="form-signin" method="post" action="{{route('sp.login')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                       
                            <div class="field-wrap">
                                <input type="text" name="mobile_email">
                                <div class="field-placeholder"><span>Mobile number or Email address</span></div>
                            </div>
                                   @if(Session::has('message'))
                            <div class="text-danger"> <i class="fas fa-exclamation-circle" style="color:danger"></i> {{ Session::get('message') }}</div>
                          @endif
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <div class="field-wrap">
                                    <input type="password" name="password" class="pass">
                                    <div class="field-placeholder"><span>Password</span></div>
                                </div>
                                @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                         @if(Session::has('password'))
                            <div class="text-danger"> <i class="fas fa-exclamation-circle" style="color:danger"></i> {{ Session::get('password') }}<br> click Forgot password to reset it.</div>
                          @endif
                            </div>
                            <div class="form-group col-auto">
                                <div class="eye" onclick="passVisibility()"><i class="fa fa-eye-slash"
                                        aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: -10px;">
                            <small class="form-text text-muted">
                                <a href="{{ url('/forgot-password') }}" style="color: gray;">Forgot Password?</a>
                            </small>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-sm btn-pill">Log in</button>
                        </div>

                        <div class="form-group text-center mb-0">
                            <hr/>
                            New to Durpalla? <a href="{{url('/registration') }}">Create Account</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
// password visibility
function passVisibility() {
    var eyebutton = document.getElementsByClassName("eye")[0];
    var pass = document.getElementsByClassName("pass")[0];

    if (pass.type === "password") {
        pass.type = "text";
    } else {
        pass.type = "password";
    }

    if (eyebutton.innerHTML.trim() === '<i class="fa fa-eye-slash" aria-hidden="true"></i>') {
        eyebutton.innerHTML = '<i class="fa fa-eye" aria-hidden="true"></i>';
    } else {
        eyebutton.innerHTML = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
    }
}

// form labels
$(function() {
    $(".field-wrap .field-placeholder").on("click", function() {
        $(this).closest(".field-wrap").find("input").focus();
    });
    $(".field-wrap input").on("keyup", function() {
        var value = $.trim($(this).val());
        if (value) {
            $(this).closest(".field-wrap").addClass("hasValue");
        } else {
            $(this).closest(".field-wrap").removeClass("hasValue");
        }
    });
});
</script>


@endsection