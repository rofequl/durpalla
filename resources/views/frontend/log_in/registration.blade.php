@extends('frontend.layout.app')
@section('content')


<section class="section-signup">

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-6 col-xl-5 mx-auto">
                <div class="form-wrapper">
                    <h3>Create a Durpalla account</h3>
                    <p>It's Quick and Easy</p>
                     <form class="form-signin" action="{{url('UserRegister')}}" method="post">
					 @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="field-wrap">
                                    <input type="text" name="fname" id="fname">
                                    <div class="field-placeholder"><span>First Name</span></div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="field-wrap">
                                    <input type="text" name="lname" id="lname">
                                    <div class="field-placeholder"><span>Last Name</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="field-wrap">
                                <input type="text" name="mobile_email" id="email" required>
                                <div class="field-placeholder"><span>Email or Phone</span></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12 col-md">
                                <div class="field-wrap">
                                    <input type="password" name="password" class="pass">
                                    <div class="field-placeholder"><span>Password</span></div>
									@if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{$error}}
                                </div>
                            @endforeach
                        @endif
                                </div>
                            </div>
                            <div class="form-group col col-md">
                                <div class="field-wrap">
                                    <input type="password" name="cpass" class="pass">
                                    <div class="field-placeholder"><span>Confirm</span></div>
                                </div>
								
                            </div>
                            <div class="form-group col-auto">
                                <div class="eye" onclick="passVisibility()"><i class="fa fa-eye-slash"
                                        aria-hidden="true"></i></div>
                            </div>
                        </div>
                        <h5>Birthday: </h5>
                        <div class="form-row">
                            <div class="form-group col">
                                <select id="inputState" name="day" class="form-control">
                                    <option value="" selected >Day</option>
                                    @for($i=1;$i<=31;$i++) <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                </select>
                            </div>
                            <div class="form-group col">
                                <select id="inputState" name="month" class="form-control">
                                    <option value="" selected >Month</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July ">July </option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November ">November </option>
                                    <option value="December">December</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <select id="inputState" name="year" class="form-control">
                                    <option value="" selected >Year</option>
                                    @for($i=1950;$i<2018;$i++) <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <h5 class="form-check-inline">Gender: </h5>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                    value="male">
                                <label class="form-check-label" for="inlineRadio1">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                    value="female">
                                <label class="form-check-label" for="inlineRadio2">Female</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <small class="form-text text-muted">
                                By clicking Sign Up, you agree to our <a href="#">Terms</a> , <a href="#">Data
                                    Policy</a> and <a href="#">Cookie Policy</a>. You may receive notifications from us
                                and can opt out at any time.
                            </small>
                        </div>
                        <div class="form-group text-center mb-0">
                            <button type="submit" id="submit" class="btn btn-success btn-sm btn-pill save">Sign Up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  
    if(localStorage) {
    $(document).ready(function() {
        $(".save").click(function() {
            // Get input name
            var fname = $("#fname").val();
              var lname = $("#lname").val();
            
            // Store data
          var x=  localStorage.setItem("fname", fname);
           var y=  localStorage.setItem("lname", lname);
          //  alert("Your first name is saved."+lname );
        });
       /* $(".access").click(function() {
            // Retrieve data
            alert("Hi, " + localStorage.getItem("lname"));
        });*/
         $('#fname').val(localStorage.getItem("fname"));
         $('#lname').val(localStorage.getItem("lname"));
         
      });
     

} else {
    alert("Sorry, your browser do not support local storage.");
}
   $('.form_error').hide();
      $('#submit').click(function(){
          // var name = $('#name').val();
           var email = $('#email').val();
           console.log(email);
            if(email== ''){
               $('#email').next().show();
               return false;
            }else{
            if(email== ''){
                $('#email').next().show();
                return false;
            }}
          

            
            //ajax call php page
           
          });
 function IsEmail(email) {
        var intRegex = /^\d+$/;
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)||(intRegex)) {
           return false;
        }else{
           return true;
        }
      }
// password visibility
function passVisibility() {
    var eyebutton = document.getElementsByClassName("eye")[0];
    var pass = document.getElementsByClassName("pass")[0];
    var cpass = document.getElementsByClassName("pass")[1];
    if (pass.type === "password") {
        pass.type = "text";
        cpass.type = "text";
    } else {
        pass.type = "password";
        cpass.type = "password";
    }

    if (eyebutton.innerHTML.trim() === '<i class="fa fa-eye-slash" aria-hidden="true"></i>') {
        eyebutton.innerHTML = '<i class="fa fa-eye" aria-hidden="true"></i>';
    } else {
        eyebutton.innerHTML = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
    }
}
// form labels
$(function() {
    $(".field-wrap .field-placeholder").on("click", function() {
        $(this).closest(".field-wrap").find("input").focus();
    });
    $(".field-wrap input").on("keyup", function() {
        var value = $.trim($(this).val());
        if (value) {
            $(this).closest(".field-wrap").addClass("hasValue");
        } else {
            $(this).closest(".field-wrap").removeClass("hasValue");
        }
    });
});
</script>


@endsection