<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\verification;
use Intervention\Image\Facades\Image;

class VerificationController extends Controller
{
    public function SpVerification()
    {

        $verification = verification::where('user_id', Session('userId'))->first();
        if (!$verification) {
            $verification = new verification;
            $verification->user_id = Session('userId');
            $verification->save();
        }
        return view('frontend.sp_panel.profile.verification', compact('verification'));
    }

    public function SpVerificationPost(Request $request)
    {
        $verification = verification::where('user_id', Session('userId'))->first();
        if ($request->submit == "nid") {

            $request->validate([
                'nid' => 'required',
                'nidImage1' => 'required',
                'nidImage2' => 'required',
            ]);
            $verification->nid = $request->nid;

            $file = new \stdClass();
            if ($file = $request->file('nidImage1')){
            $file_name = $request->nid.'_'.date('Ymd') .'_'.time() . '_1.' . $file->getClientOriginalExtension();
            $url = 'public/nid/'.$file_name;
            $file->move('public/nid', $file_name);
            $image=Image::make('public/nid/'.$file_name);
            $image->resize(300,300)->save('public/nid/'.$file_name);
            $verification->nid_image1 = $url;
            //dd($verification);       
             }
              
              $file = new \stdClass();
              if ($file = $request->file('nidImage2')){
            $file_name = $request->nid.'_'.date('Ymd') .'_'.time() . '_2.' . $file->getClientOriginalExtension();
            $url = 'public/nid/'.$file_name;
            $file->move('public/nid', $file_name);
            $image=Image::make('public/nid/'.$file_name);
            $image->resize(300,300)->save('public/nid/'.$file_name);
            $verification->nid_image2 = $url;
            }
            // dd($verification);
            $verification->nid_status = 0;
            $verification->save();
        } elseif ($request->submit == "passport") {
            $request->validate([
                'passport' => 'required',
                'passportImage1' => 'required',
                'passportImage2' => 'required',
            ]);
            $verification->passport = $request->passport;

            $file = new \stdClass();
               if ($file = $request->file('passportImage1')){
            $file_name = $request->passport.'_'.date('Ymd') .'_'.time().'_1.' . $file->getClientOriginalExtension();
            $url = 'public/passport/'.$file_name;
            $file->move('public/passport', $file_name);
            $image=Image::make('public/passport/'.$file_name);
            $image->resize(300,300)->save('public/passport/'.$file_name);
            $verification->passport_image1 = $url;
            //dd($verification);       
             }
             $file = new \stdClass();
               if ($file = $request->file('passportImage2')){
            $file_name = $request->passport.'_'.date('Ymd') .'_'.time() . '_2.' . $file->getClientOriginalExtension();
            $url = 'public/passport/'.$file_name;
            $file->move('public/passport', $file_name);
            $image=Image::make('public/passport/'.$file_name);
            $image->resize(300,300)->save('public/passport/'.$file_name);
            $verification->passport_image2 = $url;
            //dd($verification);       
             }
            $verification->passport_status = 0;
               
            $verification->save();
        } elseif ($request->submit == "driving") {
            $request->validate([
                'driving' => 'required',
                'drivingImage1' => 'required',
                'drivingImage2' => 'required',
            ]);
            $verification->driving = $request->driving;
            $file = new \stdClass();
              if ($file = $request->file('drivingImage1')){
            $file_name = $request->driving.'_'.date('Ymd') .'_'.time() . '_1.' . $file->getClientOriginalExtension();
            $url = 'public/driving/'.$file_name;
            $file->move('public/driving', $file_name);
            $image=Image::make('public/driving/'.$file_name);
            $image->resize(300,300)->save('public/driving/'.$file_name);
            $verification->driving_image1 = $url;
            //dd($verification);       
             }
          
             $file = new \stdClass();
               if ($file = $request->file('drivingImage2')){
            $file_name = $request->driving.'_'.date('Ymd') .'_'.time() . '_2.' . $file->getClientOriginalExtension();
            $url = 'public/driving/'.$file_name;
            $file->move('public/driving', $file_name);
            $image=Image::make('public/driving/'.$file_name);
            $image->resize(300,300)->save('public/driving/'.$file_name);
            $verification->driving_image2 = $url;
            //dd($verification);       
             }
             //dd($verification);      
            $verification->driving_status = 0;
            $verification->save();
        } elseif ($request->submit == "email") {
            $verification->email = $request->email;
            $verification->save();
        } elseif ($request->submit == "phone") {
            $verification->phone = $request->phone;
            $verification->save();
        } else {
            echo "Something Wrong";
        }

        return redirect('sp-verification');
    }
}
