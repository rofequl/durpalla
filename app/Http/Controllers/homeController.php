<?php

namespace App\Http\Controllers;

use App\landing_image;
use App\popular_ride;
use App\stopover;
use App\user;
use App\verification;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use DateTime;

class homeController extends Controller
{
    public function homepage()
    {

        $landingPage = landing_image::where('approve', 1)->first();
        if ($landingPage) {
            $landingPage = $landingPage->image;
        } else {
            $landingPage = false;
        }

        $popular = popular_ride::limit(3)->get();
        $rides = stopover::where('date', '>=', date("m/d/Y"))->limit(10)->get();
        return view('frontend.index', compact('landingPage', 'rides','popular'));
    }

    public function UserLogin(Request $request)
    {
        $request->validate([
           //'email' => 'required|email|exists:users,email',
           // 'phone' => 'required|phone|exists:users,phone',
            'password' => 'required|max:20|min:6',
        ]);
        $admin = user::where('phone', $request->mobile_email)
		               ->orwhere('email', $request->mobile_email)
                       ->first();
        if (!empty($admin)) {
            if ($admin && Hash::check($request->password, $admin->password)) {

                if ($request->remember_me) {
                    setcookie('userId', $admin->user_id, time() + (86400 * 30), "/");
                    setcookie('token', $admin->token, time() + (86400 * 30), "/");
                    Session::put('token', $admin->token);
                    Session::put('userId', $admin->user_id);
                } else {
                    $admin->logincount+=1;
                    Session::put('token', $admin->token);
                    Session::put('userId', $admin->user_id);
                      $admin->update();
                }
                return redirect('/')->with('status','You account is now activated. Please login.');;
            } else {
                $request->session()->flash('password', 'Wrong password. Try again or');
                return redirect('/login')->with('status','You account is now activated. Please login.');
            }
        } else {
            $request->session()->flash('message', 'Phone or Email not match');
            return redirect('/login')->with('status','You account is now activated. Please login.');
        }

    }

       public function UserRegister(Request $request)
    {
         $userId = time();
         $user = User::wherePhone($request->mobile_email)->first();
        //$phone = '0' . $request->phone;
        //dd($request->all());
        if(!is_numeric($request->get('mobile_email'))){
            // 'mobile_email' => 'required|email|unique:email|max:255',
              $request->validate([
            
            'fname' => 'required|max:191',
            'mobile_email' =>'required|email|unique:users,email|max:255',
            'day' => 'required|max:191',
            'month' => 'required|max:191',
            'year' => 'required|max:191',
            'inlineRadioOptions' => 'required|',
            'password' => 'required|between:8,255',
            'cpass' => 'required|between:8,255|same:password'
        ]); 

            }else
            {
                 if(is_numeric($request->get('mobile_email'))){
                 
                    $request->validate([
            
            'fname' => 'required|max:191',
           'mobile_email' => 'required|string|min:11|max:11|unique:users,phone',
            'day' => 'required|max:191',
            'month' => 'required|max:191',
            'year' => 'required|max:191',
            'inlineRadioOptions' => 'required|',
            'password' => 'required|between:8,255',
            'cpass' => 'required|between:8,255|same:password'
        ]); 
            }
     }
        if(!is_numeric($request->get('mobile_email'))){
             $user = new user;
        $user->email = $request->mobile_email;
        $user->name = $request->fname;
        $user->lname = $request->lname;
        $user->day = $request->day;
        $user->month = $request->month;
        $user->year = $request->year;
        $user->user_id = $userId;
        $user->gender = $request->inlineRadioOptions;
        $user->image = \URL::to('') . '/images/admin.jpg';
        $user->password = Hash::make($request->password);
        $user->token = $request->_token;
        //dd($user);
        $user->save();
            //echo "hi";
        }
        else{
                 $user = new user;
        $user->phone = $request->mobile_email;
        $user->name = $request->fname;
        $user->lname = $request->lname;
        $user->day = $request->day;
        $user->month = $request->month;
        $user->year = $request->year;
        $user->user_id = $userId;
        $user->gender = $request->inlineRadioOptions;
        $user->image = \URL::to('') . '/images/admin.jpg';
        $user->password = Hash::make($request->password);
        $user->token = $request->_token;
        //dd($user);
        $user->save();
            //echo "no";
        }
        //return "";
       

       

        Session::put('token', $request->_token);
        Session::put('userId', $userId);
        return redirect('/');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        $reg_user = user::where('facebook_id', $user->getId())->first();

        if ($reg_user) {
            echo "Already Sign up";
        } else {
            $userId = time();

            $usernew = new user;
            $usernew->name = $user->getName();
            $usernew->user_id = $userId;
            $usernew->image = $user->getAvatar();
            $usernew->facebook_id = $user->getId();
            $usernew->save();

            $verification = new verification;
            $verification->user_id = $userId;
            $verification->email = $user->getEmail();
            $verification->save();

            Session::put('userId', $userId);
            return redirect('/sp-panel');
        }

    }


    public function language(Request $request)
    {
        if (!\Session::has('locale')) {
            \Session::put('locale', $request->lng);
        } else {
            Session::put('locale', $request->lng);
        }

        return Redirect::back();
    }

    public function ForgotPassword()
    {
        return view('frontend.log_in.forget_password');
    }

    public function ForgotPasswordPost(Request $request)
    {
        $request->validate([
            'phone' => 'required|max:15',
        ]);
        $search = user::where('phone',$request->phone)->first();
        if (!empty($search)) {
            $search = user::where('phone',$request->phone)->select('name','phone')->first();
            return view('frontend.log_in.forget_password',compact('search'));
        }else{
            $request->session()->flash('message', 'Phone not match');
            return view('frontend.log_in.forget_password');
        }
    }

    public function ForgotPasswordChange(Request $request)
    {
        $request->validate([
            'password' => 'required|max:20|min:6',
            'retype_password' => 'required|max:20|min:6',
        ]);
        if ($request->password != $request->retype_password){
            $request->session()->flash('message', 'Retype password are not same');
            return view('frontend.log_in.forget_password');
        }
        $search = user::where('phone',$request->phone)->first();
        if (!empty($search)) {
            $search->password = Hash::make($request->password);
            $search->update();
            $request->session()->flash('message', 'Password change successfully');
            return redirect('/login');
        }else{
            $request->session()->flash('message', 'Phone not match');
            return view('frontend.log_in.forget_password');
        }
    }

    public function LogoutUser()
    {
        Session::forget('token');
        Session::forget('userId');
        setcookie('userId', '', time() - 3600);
        setcookie('token', '', time() - 3600);
        return redirect('/');

    }
	 public function UserReg(Request $request){
        $d= $request->all();
		if(!is_numeric($request->get('mobile-or-email'))){
			echo "hi";
		}
		else
			echo "no";
		//dd($d);
		return "";
    }
}
