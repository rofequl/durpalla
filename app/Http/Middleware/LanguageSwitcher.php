<?php

namespace App\Http\Middleware;

use App\post_ride;
use App\stopover;
use Closure;
use App;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $postId = [];
        $post = post_ride::where('status', 1)->get();
        foreach ($post as $posts) {
            array_push($postId, $posts->id);
        }
        $stopover = stopover::whereIn('post_id', $postId)->whereNotIn('status',[2,3,4])->get();

        foreach ($stopover as $stopovers) {
            $timezone = 'Asia/Dhaka';
            $input = $stopovers->date . ' ' . $stopovers->time . ' ' . $stopovers->time2;
            $date = Carbon::createFromFormat('m/d/Y h A', $input, 'Asia/Dhaka');
            $today = Carbon::parse($date, $timezone);
            $input = $stopovers->edate . ' ' . $stopovers->etime . ' ' . $stopovers->etime2;
            $date = Carbon::createFromFormat('m/d/Y h A', $input, 'Asia/Dhaka');
            $tomorrow = Carbon::parse($date, $timezone);
            $now = Carbon::now($timezone);
            if ($now->gte($today) && $now->lte($tomorrow)) {
                $update = stopover::find($stopovers->id);
                $update->status = 1;
                $update->save();

            }else if ($now->gte($tomorrow) && $now->gte($tomorrow)){
                $update = stopover::find($stopovers->id);
                $update->status = 2;
                $update->save();
            }
        }



        if(Session::get('userId') == null) {
            if (isset($_COOKIE['userId']) && isset($_COOKIE['token'])){
                Session::put('token', $_COOKIE['token']);
                Session::put('userId', $_COOKIE['userId']);
            }
        }

        App::setLocale(Session::has('locale') ? Session::get('locale') : Config::get('app.locale'));
        return $next($request);
    }
}
