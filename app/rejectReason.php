<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rejectReason extends Model
{
 protected $table = "reject_reasons";
     protected $fillable = ['reject_message'];
}
